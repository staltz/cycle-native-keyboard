import {adapt} from '@cycle/run/lib/adapt';
import xs, {Stream, Listener} from 'xstream';
import {Keyboard} from 'react-native';

export type KeyboardEventType =
  | 'keyboardWillShow'
  | 'keyboardDidShow'
  | 'keyboardWillHide'
  | 'keyboardDidHide'
  | 'keyboardWillChangeFrame'
  | 'keyboardDidChangeFrame';

function makeStream(type: KeyboardEventType): Stream<any> {
  return xs.create({
    start: function(listener: Listener<any>) {
      (this as any).keyboardCallback = (ev: any) => {
        listener.next(ev);
      };
      Keyboard.addListener(type, (this as any).keyboardCallback);
    },
    stop: function() {
      Keyboard.removeListener(type, (this as any).keyboardCallback);
    },
  });
}

export class KeyboardSource {
  constructor() {
    this._map = new Map();
    const types: Array<KeyboardEventType> = [
      'keyboardWillShow',
      'keyboardDidShow',
      'keyboardWillHide',
      'keyboardDidHide',
      'keyboardWillChangeFrame',
      'keyboardDidChangeFrame',
    ];
    for (const type of types) {
      this._map.set(type, makeStream(type));
    }
  }

  private _map: Map<KeyboardEventType, Stream<any>>;

  public events(type: KeyboardEventType): Stream<any> {
    return adapt(this._map.get(type) as Stream<any>);
  }
}

export function makeKeyboardDriver() {
  return function keyboardDriver(sink: Stream<'dismiss'>): KeyboardSource {
    sink.addListener({
      next: t => {
        Keyboard.dismiss();
      },
    });

    return new KeyboardSource();
  };
}
