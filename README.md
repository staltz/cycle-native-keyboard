# Cycle Native Keyboard

**A Cycle.js Driver for interacting with the [Keyboard](https://facebook.github.io/react-native/docs/0.55/keyboard) in React Native**

```
npm install cycle-native-keyboard
```

## Usage

### Sink

Stream of `'dismiss'` strings to force the keyboard to be dismissed.

### Source

Object with method `events(type)` which returns a stream of `type` events from the keyboard. Supported types are:

- `'keyboardWillShow'`
- `'keyboardDidShow'`
- `'keyboardWillHide'`
- `'keyboardDidHide'`
- `'keyboardWillChangeFrame'`
- `'keyboardDidChangeFrame'`

## License

Copyright (C) 2018 Andre 'Staltz' Medeiros, licensed under MIT license

